Component Cache Expiration
================================================================================

This module extends the Cache Expiration module to allow the cache for
components used to render a page to be expired when the page cache is expired
by the Cache Expiration module.

Modules in this project
--------------------------------------------------------------------------------

This project is split into two modules:

Component Cache Expiration: Provides an engine for tracking components when
they are rendered and expiring their cache when their associated page caches
are cleared.

Common Component Cache Expiration: Provides expire component plugins for
commonly used components including support for the block, views, and panels
modules.

Installation
--------------------------------------------------------------------------------

You will need the expire module in order to install this module. This is
available at https://www.drupal.org/project/expire.

For a basic installation you just need to enable the expire_components_common
module, which will automatically detect which types of components to track
and will use the default settings to track and clear them.

For more advanced configuration see below.


Configuration
--------------------------------------------------------------------------------

There are currently three major plugin types configurable through the UI at
/admin/config/system/expire under the 'Component expiration' tab.

Component: Provides support for tracking a specific type of component
and clearing its cache when the associated page cache is expired.
The expire_components_common plugin provides three component plugins for
commonly used modules:

 * Block: Provides integration for the default block system. The only
   granularity level currently supported is 'module:delta'. This means that
   all instances if a given block cache will be cleared if any page that
   renders the block is expired. This will only be an option if you have block
   caching enabled at /admin/config/development/performance.

 * Panels: Provides integration for panels. The cache plugin for a given 
   panel display will be used to clear the display cache when a page that uses
   that display is expired Any page that uses that display will have to rebuild
   the display. This will only be an option if the panels module is enabled.

 * Views: Provides integration for views. The cache plugin for the view
   display will be used to clear any data that is associated with the view
   (query and render cache) when a page that uses that view / display is
   expired. Technically, a 'view:display' granularity level is supported, but
   for default cache views plugins, performing a cache clear on any display
   wipes out the cache for every display in a given view. This means any pages
   that uses the view, regardless of display, will need to be rebuilt when the
   view is expired by this module. This will only be an option if the views
   module is enabled.

You can optionally disable a supported component plugin if you only one to
track certain types of components.

Router: Routers determine how lists of components used to render pages are
sorted into bins. When a cache expiration event occurs, the components in the
bin associated with the expired path will be expired. The more granular the
router plugin, the less likely that changes from one page will cause
components on other pages to be expired. Routers with high granularity also
require more lookup time in accordance with the number of paths a site has
since the growth of bins is O(n). Routers with low granularity tend to have
growth closer to O(1), but affect a wider group of pages when a single path
is expired. Swapping out plugins allows you to explore this trade off.

 * Store by request path: This is the default, and should be acceptable for
   most sites. It stores components on a per-request-path basis, so that only
   components used for a given request path are cleared when the path is
   expired. For a node page this might look like 'node/999'. This offers a
   moderately granular approach.

 * Store by menu path: This is the least granular approach. It stores
   component lists based on the system menu path for a given page. (For node
   pages this means 'node/%'). This is suitable for sites with a large number
   of paths and a relatively small number of components.

 * Store by request uri: This is the most granular approach. It stores
   component lists based on the system menu path for a given page. (For node
   pages this means 'node/999?filter=1'). This is suitable for sites with a
   smaller number of paths and a high number of customized components for each
   uri (This plugin should rarely be used, and is mostly provided for
   completeness.

Which router you select will effect how you write your Cache Expiration custom
urls as well. Using more granular approaches requires you to be more precise,
while using broader approaches allows you to be more general.

Storage: While routers determine the data structure of the component lists,
the storage engine determines the actual back end used to store the lists. Two
default back ends are provided:

 * Database: Uses the Drupal database API to store component lists. This is
   the default and should be used in most cases. Since the database is
   non-volatile, component lists can safely be kept for as long as necessary.
   The drawback here is the O(lgn) lookup time for component paths and
   associated load times. This method also has the benefit of supporting
   wildcard path searches from the expire module.

 * Cache: Uses the Drupal cache API to store component lists. This should be
   used only if you are running memcache, redis, or some similar caching back
   end. The goal is to increase lookups to O(1). The drawback is that wildcard
   searches are not supported and the caching back end could use volatile
   storage. Use this if you have a moderate to low level of granularity for
   component storage or you have a dedicated instance of memcache / redis for
   handling component lists. Don't use this plugin if you are relying on
   content always being fresh, as it is possible to have entries evicted from
   the cache.
