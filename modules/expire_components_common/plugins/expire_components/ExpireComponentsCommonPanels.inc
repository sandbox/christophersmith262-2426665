<?php
/**
 * @file
 * Provides Component Cache Expiration integration with the panels module.
 */

/**
 * An Expire Components plugin to support panels.
 */
class ExpireComponentsCommonPanels implements ExpireComponentsComponentInterface {

  /**
   * Only allow this plugin to be used if panels is enabled.
   */
  static public function supported() {
    return module_exists('panels');
  }

  /**
   * Creates a panels expiration handler.
   *
   * @see ExpireComponentsPluginInterface::__construct()
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
  }

  /**
   * Stores data necessary to clear the cache for a view.
   *
   * @see expire_components_common_panels_pre_render()
   * @see ExpireComponentsComponentInterface::register()
   */
  public function register($path, $component_key, $display) {
    // Only register the component if the panel is using
    // caching.
    if ($display->did && $this->cacheClearFunction($display)) {
      return array(
        'type' => 'ExpireComponentsCommonPanels',
        'did' => $display->did,
      );
    }
    else {
      return NULL;
    }
  }

  /**
   * Create a unique key for a panel.
   *
   * @see ExpireComponentsComponentInterface::getComponentKey()
   */
  public function getComponentKey($display) {
    return 'panels:' . $display->did;
  }

  /**
   * Perform cache expiration on a given panel display.
   */
  public function expire(array $data) {
    $display = panels_load_display($data['did']);
    if ($display) {
      $function = $this->cacheClearFunction($display);
      $function($display);
    }
  }

  /**
   * Get the cache clear function for a panel.
   *
   * @param object $display
   *   The panels display to get the cache clear function for.
   *
   * @return string
   *   The 'cache clear' function associated with the plugin or NULL if no cache
   *   function is found.
   */
  protected function cacheClearFunction($display) {
    ctools_include('plugins', 'panels');
    $function = NULL;
    if (isset($display->cache) && isset($display->cache['method'])) {
      $method = $display->cache['method'];
      $function = panels_plugin_get_function('cache', $method, 'cache clear');
    }
    return $function;
  }

}
