<?php
/**
 * @file
 * Provides Component Cache Expiration integration with the core block system.
 */

/**
 * An Expire Components plugin to support blocks.
 */
class ExpireComponentsCommonBlock implements ExpireComponentsComponentInterface {

  /**
   * Only allow this plugin to be used if block caching is enabled.
   */
  static public function supported() {
    return variable_get('block_cache', FALSE);
  }

  /**
   * Creates an block expiration handler.
   *
   * @see ExpireComponentsPluginInterface::__construct()
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
  }

  /**
   * Stores data necessary to clear the cache for a view.
   *
   * @see expire_components_common_panels_pre_render()
   * @see ExpireComponentsComponentInterface::register()
   */
  public function register($path, $component_key, $block) {
    // Only register the component if the block is cacheable.
    if ($this->isCacheable($block)) {
      return array(
        'type' => 'ExpireComponentsCommonBlock',
        'module' => $block->module,
        'delta' => $block->delta,
      );
    }
    else {
      return NULL;
    }
  }

  /**
   * Create a unique key for a block.
   *
   * @see ExpireComponentsComponentInterface::getComponentKey()
   */
  public function getComponentKey($block) {
    return 'block:' . $block->module . ':' . $block->delta;
  }

  /**
   * Performs cache expiration on a given block.
   */
  public function expire(array $data) {
    cache_clear_all($data['module'] . ':' . $data['delta'], 'cache_block', TRUE);
  }

  /**
   * Determines whether a block object is cacheable.
   *
   * Most of this is lifted from _block_get_cache_id()
   *
   * @param object $block
   *   A Drupal block object.
   *
   * @return bool
   *   TRUE if the block is cacheable, FALSE otherwise.
   */
  protected function isCacheable($block) {
    global $user;
    $global_cache = variable_get('block_cache', FALSE);
    $block_cache = in_array($block->cache, array(DRUPAL_NO_CACHE, DRUPAL_CACHE_CUSTOM));
    $is_root = ($user->uid == 1);
    return ($global_cache && $block_cache && !$is_root);
  }

}
