<?php
/**
 * @file
 * Provides Component Cache Expiration integration with the views module.
 */

/**
 * An Expire Components plugin to support views.
 */
class ExpireComponentsCommonViews implements ExpireComponentsComponentInterface {

  /**
   * Only allow this plugin to be used if views is enabled.
   */
  static public function supported() {
    return module_exists('views');
  }

  /**
   * Cache expiration handler for views.
   *
   * @see ExpireComponentsPluginInterface::__construct()
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
  }

  /**
   * Stores data necessary to clear the cache for a view.
   *
   * @see expire_components_common_views_pre_view()
   * @see ExpireComponentsComponentInterface::register()
   */
  public function register($path, $component_key, $data) {
    // Only register the component if the view/display is
    // using caching.
    $cache_plugin = $this->getCachePlugin($data);
    if ($cache_plugin) {
      return array(
        'type' => 'ExpireComponentsCommonViews',
      ) + $data;
    }
    else {
      return NULL;
    }
  }

  /**
   * Create a unique key for a view.
   *
   * @see ExpireComponentsComponentInterface::getComponentKey()
   */
  public function getComponentKey($data) {
    return 'views:' . $data['view'] . ':' . $data['display'];
  }

  /**
   * Perform cache expiration on a given view.
   */
  public function expire(array $data) {
    $cache_plugin = $this->getCachePlugin($data);
    if ($cache_plugin) {
      $cache_plugin->cache_flush();
    }
  }

  /**
   * Returns the cache plugin associated with a views component (display/view).
   *
   * @param array $component
   *   An array with the 'view' key specifying the name of the view and the
   *   'display' key specifying the name of the display.
   *
   * @return views_plugin_cache
   *   The views cache plugin associated with the view/display or NULL if none
   *   can be loaded.
   */
  protected function getCachePlugin(array $component) {
    $cache_plugin = NULL;
    $view = views_get_view($component['view']);
    if ($view) {
      if (isset($view->display[$component['display']])) {
        $view->set_display($component['display']);
        $display = &$view->display[$component['display']];
        if (!isset($display->display_options['cache']['type'])) {
          $display->display_options['cache']['type'] = $view->display['default']->display_options['cache']['type'];
        }
        $cache_plugin = views_get_plugin('cache', $display->display_options['cache']['type']);
        if ($cache_plugin) {
          $cache_plugin->init($view, $display);
        }
      }
    }
    return $cache_plugin;
  }

}
