<?php
/**
 * @file
 * Contains hook implementations specific to the expire_components module.
 */

/**
 * Implements hook_expire_components_plugins().
 */
function expire_components_common_expire_components_plugins() {
  $path = drupal_get_path('module', 'expire_components_common') . '/plugins/expire_components';
  return array(
    'ExpireComponentsCommonBlock' => array(
      'name' => t('Block'),
      'description' => t('Expire cached blocks on a page when the page cache is expired.'),
      'type' => 'component',
      'handler' => array(
        'class' => 'ExpireComponentsCommonBlock',
        'file' => 'ExpireComponentsCommonBlock.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsCommonPanels' => array(
      'name' => t('Panels'),
      'description' => t('Expire cached panel displays on a page when the page cache is expired.'),
      'type' => 'component',
      'handler' => array(
        'class' => 'ExpireComponentsCommonPanels',
        'file' => 'ExpireComponentsCommonPanels.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsCommonViews' => array(
      'name' => t('Views'),
      'description' => t('Expire cached views on a page when the page cache is expired.'),
      'type' => 'component',
      'handler' => array(
        'class' => 'ExpireComponentsCommonViews',
        'file' => 'ExpireComponentsCommonViews.inc',
        'path' => $path,
      ),
    ),
  );
}
