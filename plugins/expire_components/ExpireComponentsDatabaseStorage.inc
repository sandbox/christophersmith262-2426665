<?php
/**
 * @file
 * Provides the default database storage back end for storing component lists.
 */

/**
 * Uses the Drupal database API to provide a storage plugin for component lists.
 *
 * This plugin supports wild card matching.
 */
class ExpireComponentsDatabaseStorage implements ExpireComponentsStorageInterface {

  /**
   * Use of this plugin is always allowed.
   */
  static public function supported() {
    return TRUE;
  }

  /**
   * Creates a database storage plugin.
   *
   * @see ExpireComponentsPluginInterface
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
  }

  /**
   * Loads a list of components associated with a path from the database.
   *
   * @see ExpireComponentsPluginInterface::load()
   */
  public function load($path) {
    $components = db_select('expire_components', 'comp')
      ->condition('comp.path', $this->sanitize($path))
      ->fields('comp', array('components'))
      ->execute()->fetchField();
    if ($components) {
      $components = unserialize($components);
    }
    return $components;
  }

  /**
   * Saves a list of components associated with a path to the database.
   *
   * @see ExpireComponentsPluginInterface::save()
   */
  public function save($path, array $components) {
    db_merge('expire_components')
      ->key(array(
        'path' => $this->sanitize($path),
      ))
      ->fields(array(
        'components' => serialize($components),
      ))
      ->execute();
  }

  /**
   * Executes a search against the database.
   *
   * This supports the expire module '|wildcard' syntax.
   *
   * @see ExpireComponentsPluginInterface::search()
   */
  public function search(ExpireComponentsSearchQueueItem $search) {
    $components = array();
    $q = db_select('expire_components', 'comp')
      ->fields('comp', array('path', 'components'));
    $path = $this->sanitize($search->searchKey());
    if ($search->isWildcard()) {
      $q->condition('comp.path', $path . '%', 'LIKE');
    }
    else {
      $q->condition('comp.path', $path);
    }
    $results = $q->execute();
    foreach ($results as $result) {
      $components[$result->path] = unserialize($result->components);
    }
    return $components;
  }

  /**
   * Removes a list of components associated with a path from the database.
   *
   * @see ExpireComponentsPluginInterface::flush()
   */
  public function flush($path) {
    db_delete('expire_components')
      ->condition('path', $this->sanitize($path))
      ->execute();
  }

  /**
   * Removes all lists of components from the database.
   *
   * @see ExpireComponentsPluginInterface::flushAll()
   */
  public function flushAll() {
    db_delete('expire_components')
      ->execute();
  }

  /**
   * Sanitizes a path for database storage.
   *
   * The purpose here is to remove the '%' character from any paths so that we
   * can use 'LIKE' searches for lookup.
   *
   * @param string $path
   *   The path to sanitize.
   *
   * @return string
   *   A sanitized path string.
   */
  protected function sanitize($path) {
    return preg_replace('/%/', '!', $path);
  }

}
