<?php
/**
 * @file
 * Defines a class for managing expiration of cached components on a page.
 */

/**
 * Handles clearing caches for a group of components.
 *
 * Each ExpireComponentsManager object is associated with a path. We use the
 * manager object to store lists of components that were used to render a given
 * path. This is stored in the database.
 *
 * Individual plugins are responsible for detecting when a component is being
 * rendered, and for implementing the cache expiration mechanisms.
 *
 * @see ExpireComponentsComponentInterface
 *
 * When the expire module invokes hook_expire_cache() a manager object will be
 * loaded for each url being expired and the manager object will in turn get a
 * list of components that need to be expired for the url. It will then call the
 * expire() method for each component using its specified plugin.
 */
class ExpireComponentsManager implements ExpireComponentsPluginInterface, ExpireComponentsManagerInterface {
  protected $pluginData;

  /**
   * Use of this plugin is always allowed.
   */
  static public function supported() {
    return TRUE;
  }

  /**
   * Creates an ExpireComponentsManager.
   *
   * @param ExpireComponentsPluginData $plugin_data
   *   A plugin data object that has the 'path' argument set.
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
    $this->pluginData = $plugin_data;
    $this->pluginData->set('hash', $this->hash());
    if (!$this->pluginData->get('storage')) {
      $this->pluginData->set('storage', $this->pluginData->plugin('storage'));
    }
  }

  /**
   * Expires all components related to the page this cache manager operates on.
   */
  public function expireComponents() {
    $queue = new ExpireComponentsJobQueue();
    $this->eligibleComponents($queue);
    $queue->process();
  }

  /**
   * Builds a queue of expirable components.
   */
  public function eligibleComponents(ExpireComponentsJobQueue $queue) {
    foreach ($this->pluginData->get('components') as $component_key => $data) {
      if (!is_array($data)) {
        $log = format_string('Invalid plugin data');
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_PLUGIN, $this);
      }
      elseif (!isset($data['type'])) {
        $log = format_string("'type' key required for expire component plugin");
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_PLUGIN, $this);
      }
      else {
        $plugin_info = array(
          'name' => $data['type'],
          'type' => 'component',
        );
        $handler = $this->pluginData->plugin($plugin_info);
        if ($handler) {
          $item = new ExpireComponentsExpireQueueItem($component_key, $handler, $data);
          $queue->push($item);
        }
      }
    }
  }

  /**
   * Sets a component on the page this cache manager is associated with.
   *
   * @param string $component_type
   *   The type of component being added.
   * @param mixed $data
   *   Arbitrary data needed to clear the cache for the component.
   */
  public function addComponent($component_type, $data) {
    $plugin_info = array(
      'name' => $component_type,
      'type' => 'component',
    );
    $allocator = $this->pluginData->allocator($plugin_info);
    if ($allocator) {
      $plugin = $allocator->inst();
      $component_key = $plugin->getComponentKey($data);
      $component = $plugin->register($this->path(), $component_key, $data);
      if ($component) {
        $this->pluginData->add('components', $component_key, $component);
      }
    }
  }

  /**
   * Loads components from the database that have previously been saved.
   */
  public function loadComponents() {
    $path = $this->path();
    if (isset($path)) {
      $this->pluginData->set('components', $this->storage()->load($path));
      $this->pluginData->set('hash', $this->hash());
    }
  }

  /**
   * Saves components associated with this cache manager.
   */
  public function saveComponents() {
    $path = $this->path();
    if (isset($path)) {
      if ($this->hash() !== $this->pluginData->get('hash')) {
        $this->storage()->save($path, $this->components());
      }
    }
  }

  /**
   * Flushes components for this cache manager.
   */
  public function flushComponents() {
    $path = $this->path();
    if (isset($path)) {
      $this->storage()->flush($path);
    }
  }

  /**
   * Gets a list of components associated with this manager.
   *
   * @return array
   *   An array of components.
   */
  public function components() {
    $components = $this->pluginData->get('components');
    if (!$components) {
      $components = array();
    }
    return $components;
  }

  /**
   * Gets a hash of the current state of the internal components array.
   *
   * @return string
   *   An md5 hash of the serialized components array.
   */
  public function hash() {
    return md5(serialize($this->components()));
  }

  /**
   * Gets the path associated with this manager.
   *
   * @return string
   *   The path as a string.
   */
  public function path() {
    $path = $this->pluginData->get('path');
    if (!isset($path)) {
      throw new ExpireComponentsException(t('Missing path'), EXPIRE_COMPONENTS_ERR_PLUGIN, $this);
    }
    return $path;
  }

  /**
   * Gets the storage plugin to use as the data back end.
   *
   * @return ExpireComponentsStorageInterface
   *   The storage plugin for this manager.
   */
  public function storage() {
    return $this->pluginData->get('storage');
  }

}
