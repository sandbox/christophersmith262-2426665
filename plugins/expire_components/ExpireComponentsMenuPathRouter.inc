<?php
/**
 * @file
 * Provides a per-menu-path router plugin.
 */

/**
 * Implements storage on a per-menu-path basis.
 *
 * This is the key value for the entry of the page in its hook_menu
 * implementation.
 */
class ExpireComponentsMenuPathRouter extends ExpireComponentsRequestPathRouter {

  /**
   * Gets the system path for a given raw path.
   */
  public function normalize($path) {
    $parsed = drupal_parse_url(trim($path, '/'));
    $item = menu_get_item($parsed['path']);
    if ($item) {
      $normalized = $item['path'];
    }
    else {
      $normalized = $parsed['path'];
    }
    return $normalized;
  }

  /**
   * Uses the path defined in hook_menu as the current path key.
   *
   * @see ExpireComponentsMenuPathRouter::currentPathKey()
   */
  protected function currentPathKey() {
    return $this->normalize(parent::currentPathKey());
  }

}
