<?php
/**
 * @file
 * Provides a per-request-path router plugin.
 */

/**
 * Implements storage on a per-request-path basis.
 *
 * This is the request path, not including the base url or any _GET variables.
 */
class ExpireComponentsRequestPathRouter extends ExpireComponentsRouter {

  /**
   * Gets the normalized drupal path of a raw path.
   */
  public function normalize($path) {
    $parsed = drupal_parse_url(trim($path, '/'));
    return $parsed['path'];
  }

  /**
   * Uses the request_path() key as the current path key.
   *
   * @see ExpireComponentsMenuPathRouter::currentPathKey()
   */
  protected function currentPathKey() {
    return $this->normalize(request_path());
  }

}
