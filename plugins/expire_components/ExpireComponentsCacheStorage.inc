<?php
/**
 * @file
 * Provides a cache storage back end for component lists associated for a page.
 */

/**
 * Uses the Drupal cache API to provide a storage plugin for component storage.
 *
 * This plugin operates as a raw hash table, and as such, does *NOT* support
 * wildcard searches.
 *
 * @note There is no point in using this plugin if you are using the default
 * Drupal database cache as the cache back end. In that case you are better off
 * using ExpireComponentsDatabaseStorage to support wildcard lookups.
 */
class ExpireComponentsCacheStorage implements ExpireComponentsStorageInterface {

  /**
   * Only support this plugin if the cache backend isn't the database.
   */
  static public function supported() {
    $class = variable_get('cache_class_cache_expire_components');
    if (!isset($class)) {
      $class = variable_get('cache_default_class', 'DrupalDatabaseCache');
    }
    return ($class !== 'DrupalDatabaseCache');
  }

  /**
   * Creates a cache storage backend plugin.
   *
   * @see ExpireComponentsPluginInterface
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
  }

  /**
   * Loads a list of components associated with a path from the cache bin.
   *
   * @see ExpireComponentsPluginInterface::load()
   */
  public function load($path) {
    $cache = cache_get($path, 'cache_expire_components');
    if ($cache) {
      $components = unserialize($cache->data);
    }
    else {
      $components = array();
    }
    return $components;
  }

  /**
   * Use $path as the cid to store $components in the Drupal cache back end.
   *
   * @see ExpireComponentsPluginInterface::save()
   */
  public function save($path, array $components) {
    cache_set($path, serialize($components), 'cache_expire_components');
  }

  /**
   * Execute a search using an exact-match lookup in the cache back end.
   *
   * @note Wildcard searches are not supported here. This means that only a
   * single component list will ever be returned by this method.
   *
   * @see ExpireComponentsPluginInterface::search()
   */
  public function search(ExpireComponentsSearchQueueItem $search) {
    $components = $this->load($search->searchKey());
    if (!empty($components)) {
      $components = array($search->searchKey() => $components);
    }
    return $components;
  }

  /**
   * Perform a cache flush on a single path.
   */
  public function flush($path) {
    cache_clear_all($path, 'cache_expire_components');
  }

  /**
   * Perform a cache flush on the entire expire_components cache.
   */
  public function flushAll() {
    cache_clear_all('*', 'cache_expire_components', TRUE);
  }

}
