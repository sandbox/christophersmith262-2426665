<?php
/**
 * @file
 * Provides a per-uri router plugin.
 */

/**
 * Implements storage on a per-uri basis.
 *
 * This is the entire query path, without the base url, but including the _GET
 * parameters.
 */
class ExpireComponentsUriRouter extends ExpireComponentsRouter {

  /**
   * Gets the full normalized uri for a raw path.
   */
  public function normalize($path) {
    $parsed = drupal_parse_url(trim($path, '/'));
    $normalized = $parsed['path'];
    $var = '?';
    foreach ($parsed['query'] as $key => $val) {
      $normalized .= "{$var}{$key}={$val}";
      $var = '&';
    }
    if ($parsed['fragment']) {
      $normalized .= '#' . $parsed['fragment'];
    }
    return $normalized;
  }

  /**
   * Uses the request_uri() to generate the current path key.
   *
   * @see ExpireComponentsMenuPathRouter::currentPathKey()
   */
  protected function currentPathKey() {
    return $this->normalize(request_uri());
  }

}
