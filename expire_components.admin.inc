<?php
/**
 * @file
 * Provides an admin form for configuring the Expire Components module.
 */

/**
 * Admin settings form for the Expire Components module.
 */
function expire_components_admin_settings_form_elements(&$form, &$form_state) {
  $form['tabs']['expire_components'] = array(
    '#type' => 'fieldset',
    '#title' => t('Component expiration'),
    '#group' => 'tabs',
    '#weight' => 7,
  );

  $form['tabs']['expire_components']['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
  );

  // Show a field for selecting which plugins to enable.
  $options = ExpireComponents::options('component');
  $defaults = array();
  foreach ($options->options as $plugin_key => $plugin) {
    if (ExpireComponents::get('enabled_' . $plugin_key, TRUE)) {
      $defaults[] = $plugin_key;
    }
  }

  if (!empty($options->options)) {
    $item_list = array('items' => $options->description);
    $form['tabs']['expire_components']['general']['expire_components_components'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Components'),
      '#description' => t("Select which page components you would like to
        track. A list of each component used to render a page will be stored.
        When the page cache is expired for the page, the component caches used
        to render the page are also expired.") . theme('item_list', $item_list),
      '#options' => $options->options,
      '#default_value' => $defaults,
    );
  }
  else {
    $form['tabs']['expire_components']['general']['expire_components_components'] = array(
      '#markup' => t('No Expire Components plugins available.'),
    );
  }

  $options = ExpireComponents::options('router');
  $item_list = array('items' => $options->description);
  $form['tabs']['expire_components']['general']['expire_components_router'] = array(
    '#type' => 'radios',
    '#title' => t('Page Key Granularity'),
    '#description' => t("Select which cache bin router plugin you want to use.
      The router is used to create the storage key for a given page, so it
      determines the granularity of component storage.") . theme('item_list', $item_list),
    '#options' => $options->options,
    '#default_value' => $options->default,
  );

  $options = ExpireComponents::options('storage');
  $item_list = array('items' => $options->description);
  $form['tabs']['expire_components']['general']['expire_components_storage'] = array(
    '#type' => 'radios',
    '#title' => t('Storage Back End'),
    '#description' => t("Select which storage back end to use for the list.  If
      you are using a router plugin that generates a low number of total
      entries, You may be able to use the cache as a storage back end, otherwise
      use the database.") . theme('item_list', $item_list),
    '#options' => $options->options,
    '#default_value' => $options->default,
  );

  $form['#submit'][] = 'expire_components_admin_settings_form_elements_submit';
}

/**
 * Handles Expire Component admin form submissions.
 */
function expire_components_admin_settings_form_elements_submit($form, &$form_state) {
  if (isset($form_state['values']['expire_components_components'])) {
    foreach ($form_state['values']['expire_components_components'] as $plugin_key => $enabled) {
      $enabled = ($enabled) ? TRUE : FALSE;
      ExpireComponents::set('enabled_' . $plugin_key, $enabled);
    }
  }
  if (isset($form_state['values']['expire_components_router'])) {
    ExpireComponents::set('router', $form_state['values']['expire_components_router']);
  }
  if (isset($form_state['values']['expire_components_storage'])) {
    ExpireComponents::set('storage', $form_state['values']['expire_components_storage']);
  }
}
