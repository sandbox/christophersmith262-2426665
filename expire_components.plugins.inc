<?php
/**
 * @file
 * Defines plugin types and plugins available with the stock installation.
 */

/**
 * Defines the plugin types supported by the plugin API.
 */
function _expire_components_plugin_types() {
  return array(
    'manager' => array(
      'default' => 'ExpireComponentsManager',
      'allocator' => array(
        'class' => 'ExpireComponentsBinAllocator',
        'params' => array('bin_arg' => 'path'),
      ),
      'interfaces' => array('ExpireComponentsManagerInterface'),
      'parents' => array(),
    ),
    'router' => array(
      'default' => 'ExpireComponentsRequestPathRouter',
      'allocator' => 'ExpireComponentsSingletonAllocator',
      'interfaces' => array(),
      'parents' => array('ExpireComponentsRouter'),
    ),
    'storage' => array(
      'default' => 'ExpireComponentsDatabaseStorage',
      'allocator' => 'ExpireComponentsSingletonAllocator',
      'interfaces' => array('ExpireComponentsStorageInterface'),
      'parents' => array(),
    ),
    'component' => array(
      'default' => NULL,
      'allocator' => 'ExpireComponentsStandardAllocator',
      'interfaces' => array('ExpireComponentsComponentInterface'),
      'parents' => array(),
    ),
  );
}

/**
 * Lists plugins provided by default.
 */
function _expire_components_expire_components_plugins() {
  $path = drupal_get_path('module', 'expire_components') . '/plugins/expire_components';
  return array(
    'ExpireComponentsManager' => array(
      'name' => t('Basic Component Cache Manager'),
      'description' => t('The default component cache manager'),
      'type' => 'manager',
      'handler' => array(
        'class' => 'ExpireComponentsManager',
        'file' => 'ExpireComponentsManager.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsRequestPathRouter' => array(
      'name' => t('Store by request path'),
      'description' => t('Stores component lists on a per-request-path path basis (e.g., node/100).'),
      'type' => 'router',
      'handler' => array(
        'class' => 'ExpireComponentsRequestPathRouter',
        'file' => 'ExpireComponentsRequestPathRouter.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsMenuPathRouter' => array(
      'name' => t('Store by menu path'),
      'description' => t('Stores component lists on a per-menu-path path basis (What appears in hook_menu, e.g., node/%).'),
      'type' => 'router',
      'handler' => array(
        'class' => 'ExpireComponentsMenuPathRouter',
        'file' => 'ExpireComponentsMenuPathRouter.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsUriRouter' => array(
      'name' => t('Store by URI'),
      'description' => t('Stores component lists on a per-URI basis. (e.g., node/100?filter=1)'),
      'type' => 'router',
      'handler' => array(
        'class' => 'ExpireComponentsUriRouter',
        'file' => 'ExpireComponentsUriRouter.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsDatabaseStorage' => array(
      'name' => t('Database Storage'),
      'description' => t('Stores component lists in the database.'),
      'type' => 'storage',
      'handler' => array(
        'class' => 'ExpireComponentsDatabaseStorage',
        'file' => 'ExpireComponentsDatabaseStorage.inc',
        'path' => $path,
      ),
    ),
    'ExpireComponentsCacheStorage' => array(
      'name' => t('Cache Storage'),
      'description' => t("Stores component lists in the drupal cache backend
        (note this does not support wildcard matching). Be careful of volatile
        caching back ends."),
      'type' => 'storage',
      'handler' => array(
        'class' => 'ExpireComponentsCacheStorage',
        'file' => 'ExpireComponentsCacheStorage.inc',
        'path' => $path,
      ),
    ),
  );
}
