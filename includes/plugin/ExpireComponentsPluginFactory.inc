<?php
/**
 * @file
 * The plugin system is at the core of the expire components module.
 *
 * Plugins represent pieces of functionality that are swapable. The plugin
 * factory is the central hub for allowing plugins to interact with and load
 * other plugins.
 *
 * For example the 'router' plugin is used to rout component data into storage
 * tables. The 'storage' plugin is used to store the actual data.
 *
 * These two plugins can interact with each other by using the
 * ExpireComponentsPluginFactory through their associated
 * ExpireComponentsPluginData.
 */

/**
 * Defines a plugin factory which is used to create and load plugin objects.
 *
 * Each plugin has a ctools info array associated with it that describes its
 * plugin type and implementing class.
 *
 * The expire component plugin factory allows end users to set which particular
 * plugin implementation is 'active' for a given type. This is the plugin that
 * will be used by default when other objects request an object of that type. If
 * no active plugin is set for a given type, the default, which is set in the
 * plugin type info array is used.
 *
 * The plugin factory allows different plugin types to use different allocation
 * schemes through the ExpireComponentsAllocatorInterface.
 *
 * @see ExpireComponentsAllocatorInterface.
 */
class ExpireComponentsPluginFactory {
  protected $plugins;
  protected $types;
  protected $inUse;

  /**
   * Creates an expire components plugin object.
   *
   * @param array $plugin_types
   *   An associative array describing the plugin types to be supported by the
   *   plugin factory.
   *
   * @see _expire_components_plugin_types()
   */
  public function __construct($plugin_types = array()) {
    // Create allocator prototypes for each plugin type.
    foreach ($plugin_types as $plugin_type => $info) {
      $this->plugins[$plugin_type] = array();
      $plugin_types[$plugin_type]['allocator'] = $this->createAllocatorPrototype($plugin_type, $info['allocator']);
    }
    // Load the actual plugins.
    $this->types = $plugin_types;
    $this->inUse = array();
    $this->plugins = $this->loadPlugins();
    // Make sure there is at least one valid plugin available
    // for each plugin type.
    foreach ($this->types as $plugin_type => $info) {
      if (empty($this->plugins[$plugin_type])) {
        $log = format_string('No plugins available for type @type', array('@type' => $plugin_type));
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
      }
    }
  }

  /**
   * Gets an allocator object for getting plugin instances.
   *
   * If you want to create a plugin from the factory, you want to call this
   * method. Allocators are created for a specific plugin implementation by
   * copying the plugin allocator prototype for a plugin type and providing it
   * with additional context (a plugin name) that allows it to create a specific
   * type of plugin.
   *
   * @param mixed $plugin
   *   If this is a string then it is treated as the type of plugin to get an
   *   allocator for. If this is an array it must have a 'type' key which should
   *   hold the type of plugin being requested, and an optional 'name' key which
   *   will specify the particular plugin to get the allocator for. If only a
   *   plugin type is provided, the factory will attempt to create an allocator
   *   for the active plugin for the requested type, with the default for a
   *   plugin type being used as the fallback.
   *
   * @return ExpireComponentsAllocator
   *   An object that implements ExpireComponentsAllocatorInstance and generates
   *   plugin objects of a specifc type.
   */
  public function getAllocator($plugin) {
    // Convert non-array input to array format.
    if (!is_array($plugin)) {
      $plugin_type = $plugin;
      $plugin = array('type' => $plugin_type);
    }
    // What type of plugin to get an allocator for?
    if (!isset($plugin['type'])) {
      $log = format_string('Malformed plugin identifier passed');
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    // If we aren't specifically given a plugin
    // implementation to use, then we use the
    // active plugin for the plugin type.
    if (!isset($plugin['name'])) {
      $plugin['name'] = $this->activePlugin($plugin['type']);
    }
    return $this->buildAllocator($plugin['type'], $plugin['name']);
  }

  /**
   * Gets an array of plugin info arrays.
   *
   * @param string $plugin_type
   *   If this is NULL then all plugin types are fetched, if non-NULL then only
   *   plugin info arrays for the requested plugin type are fetched.
   *
   * @return array
   *   If $plugin_type is NULL, an array keyed by plugin type is returned where
   *   each sub array is keyed by plugin name and contains a ctools info array
   *   for the plugin. If $plugin_type is non-NULL, an array keyed by plugin
   *   name is returned, where values are the ctools info array for the plugin.
   */
  public function plugins($plugin_type = NULL) {
    if (isset($plugin_type)) {
      if (isset($this->plugins[$plugin_type])) {
        $plugins = $this->plugins[$plugin_type];
      }
      else {
        // Nobody should ever be looking up plugins for
        // a plugin type that doesn't exists.
        $log = format_string('Invalid plugin type @type', array('@type' => $plugin_type));
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
      }
    }
    else {
      $plugins = $this->plugins;
    }
    return $plugins;
  }

  /**
   * Gets the ctools plugin info array for a given plugin.
   *
   * @param string $plugin_type
   *   The type of plugin being looked up.
   * @param string $plugin_key
   *   The unique name of the plugin being looked up.
   *
   * @return array
   *   A ctools plugin info array for the given plugin or NULL if the plugin
   *   could not be found.
   */
  public function pluginInfo($plugin_type, $plugin_key) {
    $info = NULL;
    $plugins = $this->plugins($plugin_type);
    if ($plugins) {
      if (isset($plugins[$plugin_key])) {
        $info = $plugins[$plugin_key];
      }
    }
    return $info;
  }

  /**
   * Gets the type info associated with a plugin type.
   *
   * This contains information about default values, allocator schemes, required
   * interfaces, etc.
   *
   * @param string $plugin_type
   *   The type to get info about.
   *
   * @return array
   *   The plugin type info array.
   *
   * @see _expire_components_plugin_types()
   */
  public function typeInfo($plugin_type) {
    if (isset($this->types[$plugin_type])) {
      $info = $this->types[$plugin_type];
    }
    else {
      // Nobody should be inquiring about plugin types that
      // aren't associated with the factory.
      $log = format_string('Invalid plugin type @type', array('@type' => $plugin_type));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    return $info;
  }

  /**
   * Uses the ctools plugin API to get the class name for a given plugin.
   *
   * @param string $plugin_type
   *   The type of plugin to get the class for.
   * @param string $plugin_key
   *   The name of the plugin to get the class for.
   *
   * @return string
   *   If an instantiable class is found, then the class name is returned,
   *   otherwise NULL is returned.
   */
  public function pluginClass($plugin_type, $plugin_key) {
    ctools_include('plugins');
    return ctools_plugin_load_class('expire_components', 'plugins', $plugin_key, 'handler');
  }

  /**
   * Tells whether a particular plugin implementation exists.
   *
   * @param string $plugin_type
   *   The plugin type of the plugin to check for.
   * @param string $plugin_key
   *   The specific plugin to check for.
   *
   * @return bool
   *   TRUE if the plugin was loaded, FALSE otherwise.
   */
  public function pluginExists($plugin_type, $plugin_key) {
    return isset($this->plugins[$plugin_type])
      && isset($this->plugins[$plugin_type][$plugin_key]);
  }

  /**
   * Sets the active plugin for a given plugin type.
   *
   * @param string $plugin_type
   *   The plugin type to set the active plugin for.
   * @param string $plugin_key
   *   The name of the plugin that will be used as the active plugin for the
   *   given plugin type.
   */
  public function usePlugin($plugin_type, $plugin_key) {
    if ($plugin_type && $plugin_key) {
      if (!$this->pluginExists($plugin_type, $plugin_key)) {
        $log = format_string('Plugin not found @name', array('@name' => $plugin_key));
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
      }
      $this->inUse[$plugin_type] = $plugin_key;
    }
  }

  /**
   * Gets the plugin that is active for a given plugin type.
   *
   * If a plugin was set by the usePlugin() method, then that plugin is used. If
   * no plugin was set in that fashion, then we fall back to the default for the
   * plugin type.
   *
   * @param string $plugin_type
   *   The type of plugin to get the active plugin implementation for.
   *
   * @return string
   *   The name of the active plugin, or default plugin fallback.
   */
  public function activePlugin($plugin_type) {
    $plugin_key = NULL;
    // Prefer to use the plugin implementation that
    // was actively set by a caller, but fall back
    // to the default for the plugin type if available.
    if (isset($this->inUse[$plugin_type])) {
      $plugin_key = $this->inUse[$plugin_type];
    }
    elseif (isset($this->types[$plugin_type])) {
      if (isset($this->types[$plugin_type]['default'])) {
        $plugin_key = $this->types[$plugin_type]['default'];
      }
    }
    // If there is active plugin set AND no default plugin
    // set for a given plugin type, then something went
    // really wrong in the factory.
    if (!$plugin_key) {
      $log = format_string('No active plugin for type @type', array('@type' => $plugin_type));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    return $plugin_key;
  }

  /**
   * Builds a an allocator prototype for a specific plugin.
   *
   * @param string $plugin_type
   *   The type of plugin to create the allocator for. This will determine which
   *   allocator prototype is used.
   * @param string $plugin_key
   *   The name of the specific plugin to create the allocator for. This will
   *   determine the specific class that the allocator will generate objects.
   *
   * @return ExpireComponentsAllocatorInterface
   *   An allocator object that can be used to create or load plugin instances.
   */
  protected function buildAllocator($plugin_type, $plugin_key) {
    $plugin = $this->pluginInfo($plugin_type, $plugin_key);
    if (!$plugin) {
      $log = format_string('Plugin does not exist @key', array('@key' => $plugin_key));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    $class = $this->pluginClass($plugin_type, $plugin_key);
    if (!$class) {
      $log = format_string('Plugin class could not be found @key', array('@key' => $plugin_key));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    if (!isset($this->types[$plugin_type]['allocator'])) {
      $log = format_string('Plugin has no valid allocator @key', array('@key' => $plugin_key));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    $prototype = $this->types[$plugin_type]['allocator'];
    $plugin_data = new ExpireComponentsPluginData($class, $plugin, $this);
    return $prototype->copyFromPrototype($plugin_data);
  }

  /**
   * Creates a general allocator prototype for a general plugin type.
   *
   * @param string $plugin_type
   *   The type of plugin to create the allocator for.
   * @param mixed $info
   *   If this is a string then is treated as the allocator class name to use.
   *   If it is an array then the 'class' key is used as the allocator class
   *   name and the 'params' key is used to attach parameters to the allocator
   *   type.
   *
   * @return ExpireComponentsAllocator
   *   A generic allocator object for a given type.
   */
  protected function createAllocatorPrototype($plugin_type, $info) {
    if (is_array($info)) {
      // Enforce correct array key existence.
      if (!isset($info['class']) || !isset($info['params'])) {
        $log = format_string('Malformed plugin info passed');
        throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
      }
      $class = $info['class'];
      $params = $info['params'];
    }
    else {
      $class = $info;
      $params = array();
    }
    // We should never be getting a class that can't
    // be instantiated here.
    if (!class_exists($class)) {
      $log = format_string('Invalid class given @class', array('@class' => $class));
      throw new ExpireComponentsException($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
    }
    // Set any additional necessary parameters.
    $allocator = new $class($plugin_type);
    foreach ($params as $key => $val) {
      $allocator->setParam($key, $val);
    }
    return $allocator;
  }

  /**
   * Gets the requirements for implementations of a plugin.
   *
   * @param string $plugin_type
   *   The type of plugin to get requirements for.
   * @param string $requirement
   *   The type of requirement to get.
   *
   * @return array
   *   An array of strings associated with a requirement.
   */
  protected function required($plugin_type, $requirement) {
    if (isset($this->types[$plugin_type])) {
      $requirements = $this->types[$plugin_type][$requirement];
    }
    else {
      $requirements = array();
    }
    return $requirements;
  }

  /**
   * Loads the plugin info array for each plugin using the ctools plugin API.
   *
   * @return array
   *   An array, keyed by plugin type, where each subarray is a list of plugin
   *   info arrays for the given type, keyed by their plugin names.
   */
  protected function loadPlugins() {
    ctools_include('plugins');
    $loaded_plugins = ctools_get_plugins('expire_components', 'plugins');
    $processed_plugins = array();
    foreach ($loaded_plugins as $plugin_key => $plugin) {
      if (isset($plugin['type'])) {
        // Only load plugins that can be instantiated and
        // implement the required methods for their plugin
        // type.
        if (self::validatePlugin($plugin['type'], $plugin_key)) {
          $class = $this->pluginClass($plugin['type'], $plugin_key);
          if ($class::supported()) {
            $plugin['enabled'] = ExpireComponents::get('enabled_' . $plugin_key, TRUE);
            $processed_plugins[$plugin['type']][$plugin_key] = $plugin;
          }
        }
      }
      else {
        $log = format_string('Plugin definition missing type key');
        ExpireComponents::watchdog($log, EXPIRE_COMPONENTS_ERR_FACTORY, $this);
      }
    }
    return $processed_plugins;
  }

  /**
   * Checks that a plugin is valid given its plugin type.
   *
   * @param string $plugin_type
   *   The type of plugin to validate.
   * @param string $plugin_key
   *   The name of the plugin to validate.
   *
   * @return bool
   *   TRUE if the plugin class can be instantiated, it implements the required
   *   interfaces, and has the required ancestors, or FALSE if one of those
   *   conditions does not pass.
   */
  protected function validatePlugin($plugin_type, $plugin_key) {
    // Issues with validation should not cause exceptions
    // to get thrown. We handle the case where no valid
    // plugins exist elsewhere. At this point, we just
    // want to log when we encounter a malformed plugin
    // definition.
    $class = $this->pluginClass($plugin_type, $plugin_key);
    // Check that the plugin can be instantiated.
    $info = array(
      '@class' => $class,
    );
    if (!$class || !class_exists($class)) {
      ExpireComponents::watchdogWarn('Could not load plugin: @class does not exist', $info);
      return FALSE;
    }
    // Check that the plugin implements the required interfaces
    // for its plugin type.
    $interfaces = class_implements($class);
    foreach ($this->required($plugin_type, 'interfaces') as $interface) {
      if (!isset($interfaces[$interface])) {
        $info['@interface'] = $interface;
        ExpireComponents::watchdogWarn('Could not laod plugin: @class does not implement @interface', $info);
        return FALSE;
      }
    }
    // Check that the plugin has the required ancestors.
    foreach ($this->required($plugin_type, 'parents') as $parent_class) {
      if (!is_subclass_of($class, $parent_class, TRUE)) {
        $info['@parent'] = $parent_class;
        ExpireComponents::watchdogWarn('Could not load plugin: @class does inherit from @parent', $info);
        return FALSE;
      }
    }
    return TRUE;
  }

}
