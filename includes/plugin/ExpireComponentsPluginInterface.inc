<?php
/**
 * @file
 * Defines the plugin interface that all plugins adhere to.
 */

/**
 * A class implementing this can be used in the plugin system.
 *
 * @note You probably never want to implement this directly. You should probably
 * be implementing an inheriting interface or base class.
 *
 * @todo Add support for built in forms / settings
 */
interface ExpireComponentsPluginInterface {

  /**
   * Should return TRUE if this plugin can be enabled, FALSE otherwise.
   *
   * This allows you to prevent plugins from being enabled in situations where
   * they may cause more harm than good, or flat out won't work.
   */
  static public function supported();

  /**
   * This provides the magic that makes the plugin allocator engine work.
   *
   * Each plugin is initialized with plugin data that includes context for the
   * plugin being created.
   *
   * This allows the dynamic creation of instantiation calls and arguments.
   */
  public function __construct(ExpireComponentsPluginData $data);

}
