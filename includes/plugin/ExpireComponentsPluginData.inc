<?php
/**
 * @file
 * The plugin data architecture is used to pass arbitrary data between plugins.
 *
 * This provides a single unified interface for instantiating plugins with
 * arbitrary data while enforcing a strict prototype on the constructor
 * function signature for plugins.
 */

/**
 * Contains the data associated with a plugin.
 *
 * This will be used to create the plugin and provide access for the plugin to
 * get its settings and parameters.
 */
class ExpireComponentsPluginData {
  protected $plugins;
  protected $args;
  protected $info;
  protected $class;

  /**
   * Creates a plugin data object.
   *
   * @param string $class
   *   The class name of the plugin that this data belongs to.
   * @param array $plugin
   *   Information about the plugin defined in its ctools info array.
   * @param ExpireComponentsPluginFactory $plugins
   *   The plugin factory used to load the plugin that can be used to load
   *   additional plugins.
   */
  public function __construct($class, array $plugin, ExpireComponentsPluginFactory $plugins) {
    $this->class = $class;
    $this->info = $plugin;
    $this->plugins = $plugins;
    $this->args = array();
  }

  /**
   * Sets an argument for the plugin.
   *
   * @param string $key
   *   The name of the argument to set.
   * @param mixed $val
   *   The value of the argument to set.
   */
  public function set($key, $val) {
    $this->args[$key] = $val;
  }

  /**
   * Adds a value to an argument that is an array.
   *
   * If the provided key does not represent an object that is stored as an array
   * the current contents will be overwritten.
   *
   * @param string $key
   *   The name of the argument to add to.
   * @param string $assoc_key
   *   The key to set in the array.
   * @param mixed $assoc_value
   *   The value to set in the array.
   */
  public function add($key, $assoc_key, $assoc_value) {
    if (!is_array($this->args[$key])) {
      $this->set($key, array());
    }
    $this->args[$key] += array(
      $assoc_key => $assoc_value,
    );
  }

  /**
   * Gets a plugin argument value.
   *
   * @param string $key
   *   The name of the argument to get. To get all arguments associated with the
   *   plugin pass $key = NULL.
   *
   * @return mixed
   *   The argument value associated with a key. If the $key is NULL, this is an
   *   array of all arguments associated with the plugin. If $key is not NULL,
   *   and the key could not be found, NULL is returned.
   */
  public function get($key = NULL) {
    if (isset($key)) {
      return isset($this->args[$key]) ? $this->args[$key] : NULL;
    }
    else {
      return $this->args;
    }
  }

  /**
   * Gets the plugin info array for the associated  plugin.
   *
   * @return array
   *   A ctools plugin info array.
   */
  public function getPluginInfo() {
    return $this->info;
  }

  /**
   * Gets an allocator for a plugin.
   *
   * @param mixed $plugin
   *   If $plugin is a string then it is treated as the plugin type to look up.
   *   If it is an array then it should contain the 'type' key, which is the
   *   plugin type to lookup. If it additionally defined a 'name' key, then the
   *   'name' key is used to attempt to load a particular implementation of a
   *   given plugin type.
   *
   * @return ExpireComponentsAllocator
   *   An allocator object implementing ExpireComponentsAllacatorInterface that
   *   can be used to allocator the requested type of plugin.
   *
   * @note The reason to use this method instead of the plugin() method is if
   * you want to minipulate the default arguments for the plugin before
   * instantiating, otherwise use the plugin() wrapper method.
   */
  public function allocator($plugin) {
    return $this->plugins->getAllocator($plugin);
  }

  /**
   * This is a wrapper for the allocator() to instantiate plugins.
   *
   * @return ExpireComponentsPluginInterface
   *   An object implementing ExpireComponentsPluginInterface.
   *
   * @see allocator()
   */
  public function plugin($plugin) {
    return $this->allocator($plugin)->inst();
  }

  /**
   * Gives read access to the class of the plugin this data is associated with.
   *
   * @return string
   *   The class name this data is associated with.
   */
  public function type() {
    return $this->class;
  }

}
