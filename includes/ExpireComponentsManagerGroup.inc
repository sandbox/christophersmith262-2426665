<?php
/**
 * @file
 * Provides a composite for dealing with manager plugins.
 */

/**
 * Implementation of a composite class managers.
 *
 * Using this class allows methods to return either a single manager plugin, or
 * a group of manager plugins for a path lookup.
 */
class ExpireComponentsManagerGroup implements ExpireComponentsManagerInterface {
  protected $managers;

  /**
   * Creates an ExpireComponentsManagerGroup.
   */
  public function __construct($managers = array()) {
    $this->managers = $managers;
  }

  /**
   * Adds a manager to the group.
   *
   * @param string $path
   *   The path associated with the manager being inserted.
   * @param ExpireComponentsManagerInterface $manager
   *   The manager to insert.
   */
  public function insert($path, ExpireComponentsManagerInterface $manager) {
    if ($manager) {
      $this->managers[$path] = $manager;
    }
  }

  /**
   * Removes an item from the internal list of managers.
   *
   * @param string $path
   *   The path to remove the manager for.
   */
  public function remove($path) {
    unset($this->managers[$path]);
  }

  /**
   * Expires components for all managers grouped by this object.
   */
  public function expireComponents() {
    $expire_queue = new ExpireComponentsJobQueue();
    // Many different pages may share a single component so
    // we go through each manager and allow it to add its
    // components to the queue. If the component is already
    // in the queue it will overwrite the existing entry.
    // After all components have been added we process the
    // queue all at once.
    foreach ($this->managers as $manager) {
      $manager->eligibleComponents($expire_queue);
    }
    $expire_queue->process();
  }

  /**
   * Aggregates the eligible components for each manger.
   *
   * @see ExpireComponentsManagerInterface::eligibleComponents()
   */
  public function eligibleComponents(ExpireComponentsJobQueue $queue) {
    foreach ($this->managers as $manager) {
      $manager->eligibleComponent($queue);
    }
  }

  /**
   * Adds a component to each manger.
   *
   * @see ExpireComponentsManagerInterface::addComponent()
   */
  public function addComponent($plugin_type, $data) {
    foreach ($this->managers as $manager) {
      $manager->addComponent($plugin_type, $data);
    }
  }

  /**
   * Aggregates the components list for each manager.
   *
   * @see ExpireComponentsManagerInterface::loadComponents()
   */
  public function loadComponents() {
    $components = array();
    foreach ($this->managers as $manager) {
      $components += $manager->loadComponents();
    }
    return $components;
  }

  /**
   * Saves the components list for each manager.
   *
   * @see ExpireComponentsManagerInterface::saveComponents()
   */
  public function saveComponents() {
    foreach ($this->managers as $manager) {
      $manager->saveComponents();
    }
  }

  /**
   * Flushes the components for each manager.
   *
   * @see ExpireComponentsManagerInterface::flushComponents()
   */
  public function flushComponents() {
    foreach ($this->managers as $manager) {
      $manager->flushComponents();
    }
  }

}
