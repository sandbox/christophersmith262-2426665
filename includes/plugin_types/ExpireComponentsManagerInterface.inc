<?php
/**
 * @file
 * Defines an interface for creating manager plugins.
 */

/**
 * All manager plugins should implement this interface.
 *
 * The job of a manager plugin is to manage the component caches for a specific
 * url.
 *
 * The manager is in charge of proctoring out component list I/O calls with the
 * storage back end, and for informing component plugins to clear the cache for
 * a given component.
 *
 * @note its more likely you want to subclass ExpireComponentsManager, which is
 * the default component manager, rather than completely implementing a new
 * manager based on this interface.
 */
interface ExpireComponentsManagerInterface {
  /**
   * Expire all components related to this component manager.
   *
   * This is in charge of getting the component plugin for each component
   * related to this manager and performing the expiration action.
   */
  public function expireComponents();

  /**
   * Queues a list of component expiration jobs.
   *
   * Each queue item contains handles a single component and contains a list of
   * data needed to expire the component.
   *
   * Items that are eligible to be expired should be added to the queue by the
   * implemented method.
   *
   * @param ExpireComponentsJobQueue $queue
   *   The queue of components that can be expired.
   */
  public function eligibleComponents(ExpireComponentsJobQueue $queue);

  /**
   * Adds a component to this manager's list.
   *
   * @param string $plugin_type
   *   The component type (which is a specific key for a component plugin) that
   *   should be used to handle the data.
   * @param mixed $data
   *   Arbitrary data that can later be used to expire the component.
   */
  public function addComponent($plugin_type, $data);

  /**
   * Responsible for filling in an internal list of components for this manager.
   */
  public function loadComponents();

  /**
   * Responsible for saving the internal list of components for this manager.
   */
  public function saveComponents();

  /**
   * Responsible for clearing out the component list for this manager.
   */
  public function flushComponents();

}
