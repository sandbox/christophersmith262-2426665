<?php
/**
 * @file
 * Provides a base class for all router plugins.
 */

/**
 * All router plugins should inherit from this abstract base class.
 *
 * It provides the basic default functionality which can be overwridden by a
 * concrete plugin implementation.
 */
abstract class ExpireComponentsRouter implements ExpireComponentsPluginInterface {
  protected $pluginData;

  /**
   * By default we allow any router plugin to be used.
   */
  static public function supported() {
    return TRUE;
  }

  /**
   * Creates an ExpireComponentsRouter object.
   *
   * @param ExpireComponentsPluginData $plugin_data
   *   Plugin data to be stored internally.
   */
  public function __construct(ExpireComponentsPluginData $plugin_data) {
    $this->pluginData = $plugin_data;
  }

  /**
   * Gets a manager class instance for the current path.
   *
   * @return ExpireComponentManagerInterface
   *   A manager class instance.
   */
  public function currentPath() {
    return $this->getManagerFor($this->currentPathKey());
  }

  /**
   * Gets a manager for each match path in an array of paths.
   *
   * @param array $paths
   *   An array of paths to get managers for.
   *
   * @return ExpireComponentsManagerGroup
   *   An object which is a composite of all managers that matched the requested
   *   paths.
   */
  public function lookup(array $paths) {
    $managers = new ExpireComponentsManagerGroup();
    // Enable path to work on a single path or an array of paths.
    if (!is_array($paths)) {
      $paths = array($paths);
    }
    // Queue items based on their path. Using a job-id
    // based approach allows us to only lookup components
    // for each path once (in case multiple expire paths
    // normalize to the same path).
    $search_queue = new ExpireComponentsJobQueue();
    $storage = $this->pluginData->plugin('storage');
    foreach ($paths as $path) {
      $item = new ExpireComponentsSearchQueueItem($path, $this, $storage);
      $search_queue->push($item);
    }
    // Process the queue and create a manager for each
    // path / component pair that was returned.
    foreach ($search_queue->process() as $result) {
      foreach ($result as $path => $components) {
        $manager = $this->getManagerFor($path, $components);
        $managers->insert($path, $manager);
      }
    }
    return $managers;
  }

  /**
   * Gets a manager plugin instance for a given path.
   *
   * @param string $path
   *   The path to create the manager for.
   * @param array $components
   *   An array of components to initialize the manager with.
   *
   * @return ExpireComponentManagerInterface
   *   A manager class instance.
   */
  protected function getManagerFor($path, $components = array()) {
    $allocator = $this->pluginData->allocator('manager');
    $allocator->args()->set('path', $path);
    $allocator->args()->set('components', $components);
    return $allocator->inst();
  }

  /**
   * Gets the normalized version of a path.
   *
   * This method should take a raw path from an expire request, or from a
   * generic path lookup, and return a normalized version of the path.
   *
   * In other words this should generate path keys that are
   * to associate components with the pages they appear on.
   *
   * @param string $path
   *   The path to be normalized.
   *
   * @return string
   *   A string representing the normalized path.
   */
  abstract public function normalize($path);

  /**
   * This method should return a unique path key for the current page request.
   *
   * Component lists are stored on a per-path-key basis, so this determines the
   * granularity of cache clears.
   *
   * @return string
   *   A string representing the path key for the current page.
   */
  abstract protected function currentPathKey();

}
