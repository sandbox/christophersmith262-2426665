<?php
/**
 * @file
 * Defines an interface for storage plugins.
 */

/**
 * Every storage plugin should implement this interface.
 *
 * The role of a storage plugin is to handle persistent storage of component
 * lists.
 *
 * The individual concrete plugin implementation is responsible for the low
 * level I/O calls.
 */
interface ExpireComponentsStorageInterface extends ExpireComponentsPluginInterface {

  /**
   * Loads a component list for a path from the storage back end.
   *
   * @param string $path
   *   The path to load the component list for.
   *
   * @return array
   *   An array, keyed by component keys, where values are component data that
   *   can be used to clear the component cache.
   */
  public function load($path);

  /**
   * Saves a component list for a path to the storage back end.
   *
   * @param string $path
   *   The path to associate the components with.
   * @param array $components
   *   An associative array keyed by component keys where the values are arrays
   *   of data that can later be used to clear the component cache.
   */
  public function save($path, array $components);

  /**
   * Executes a search object by finding results that match the search key.
   *
   * @param ExpireComponentsSearchQueueItem $search
   *   A search object to be processed. A search object contains a search key to
   *   be matched against.
   *
   * @return array
   *   An array, keyed by component keys, where the values are arrays of data
   *   that can be used to clear the component cache.
   */
  public function search(ExpireComponentsSearchQueueItem $search);

  /**
   * Deletes all components associated with a path from the storage back end.
   *
   * @param string $path
   *   The path to delete the component list for.
   */
  public function flush($path);

  /**
   * Deletes all component lists from the storage back end.
   */
  public function flushAll();

}
