<?php
/**
 * @file
 * Defines the interface for expire component plugins.
 */

/**
 * Each component plugin should implement this interface.
 *
 * The role of a class implementing this interface is determine what type of
 * data needs to be stored to:
 *
 * 1. Determine what data is needed to clear component caches
 * 2. Differentiate between components on a page
 * 3. Perform the expiration of the components.
 *
 * Each plugin should represent a type of cacheable component on a page and
 * should implement the back end logic needed to clear the cache for that
 * particular type of component (e.g., views, panels, etc).
 *
 * @see the expire_components_views module for an example of how to define
 * plugins and make the Expire Components module aware of them.
 */
interface ExpireComponentsComponentInterface extends ExpireComponentsPluginInterface {

  /**
   * Provides the ability for plugins to define their own support.
   *
   * @return bool
   *   TRUE if the plugin can be loaded, FALSE otherwise.
   */
  static public function supported();

  /**
   * Builds a list of data required purge the cache for a specific component.
   *
   * The component is uniquely identified by its $component_key.
   *
   * @param string $path
   *   The path that the component is associated with.
   * @param string $component_key
   *   The unique key for the component within the path scope.
   * @param mixed $data
   *   An arbitrary data structure representing the component.
   *
   * @return array
   *   The final data to be saved for the component or NULL if the component is
   *   not supported.
   */
  public function register($path, $component_key, $data);

  /**
   * Builds a unique key to represent the component within the scope of a page.
   *
   * @param mixed $data
   *   An arbitrary data structure representing the component.
   *
   * @return string
   *   A unique key for a component.
   */
  public function getComponentKey($data);

  /**
   * Perform a cache expiration on a component.
   *
   * @param array $data
   *   An associative array containing the data that was created by the
   *   register() method.
   */
  public function expire(array $data);

}
