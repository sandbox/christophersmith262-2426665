<?php
/**
 * @file
 * Provides allocator support for the singleton pattern.
 *
 * This is done (hopefully) in a way that makes the singleton pattern suck less.
 */

/**
 * A class to implement the singleton creational pattern for plugins.
 */
class ExpireComponentsSingletonAllocator extends ExpireComponentsAllocator {
  protected static $inst = array();

  /**
   * For each plugin type we only ever allocate one instance.
   */
  public function allocate() {
    $class = $this->pluginData->type();
    if (!isset(self::$inst[$class])) {
      self::$inst[$class] = $this->create();
    }
    return self::$inst[$class];
  }

}
