<?php
/**
 * @file
 * Defines an allocator class for a 'bin' based allocation scheme.
 */

/**
 * Implements an allocator for returning objects based on a given piece .
 *
 * The allocator is created with a 'bin_arg' param that tells the allocator the
 * ExpireComponentsPluginData entry that will be used to create bins.
 *
 * Bins are created on a per-type basis. For a given plugin type, there is at
 * most one object in its bin. If the allocator is called to retrieve an object
 * and there is an entry in the 'bin' corresponding to the plugin data, then the
 * object already in that bin is returned. Otherwise we create a new object and
 * stick it in the bin.
 *
 * @see ExpireComponentsAllocator
 */
class ExpireComponentsBinAllocator extends ExpireComponentsAllocator {
  static protected $bins = array();

  /**
   * Performs bin-based allocation.
   */
  public function allocate() {
    $inst = NULL;
    $key = $this->getBinKey();
    if (isset($key)) {
      // Create the plugin instance if it doesn't already exist.
      if (!isset(self::$bins[$this->pluginType])) {
        self::$bins[$this->pluginType] = array();
      }
      // This should throw an exception if the plugin couldn't
      // be created.
      if (!isset(self::$bins[$this->pluginType][$key])) {
        self::$bins[$this->pluginType][$key] = $this->create();
      }
      // Return the plugin instance associated with the bin key.
      $inst = self::$bins[$this->pluginType][$key];
    }
    return $inst;
  }

  /**
   * Retrieves the key for a plugin type that determines where it is stored.
   *
   * @return mixed
   *   An ExpireComponentsPluginData key.
   */
  protected function getBinKey() {
    $bin_key = $this->getParam('bin_arg');
    if (isset($bin_key)) {
      $key = $this->args()->get($bin_key);
    }
    else {
      $key = NULL;
    }
    if (!isset($key)) {
      throw new ExpireComponentsException('Invalid bin key', EXPIRE_COMPONENTS_ERR_ALLOC, $this);
    }
    return $key;
  }

}
