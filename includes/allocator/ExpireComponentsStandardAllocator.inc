<?php
/**
 * @file
 * Provides the basic expected allocator.
 */

/**
 * Defines an allocator which returns a unique instance per allocation request.
 */
class ExpireComponentsStandardAllocator extends ExpireComponentsAllocator {

  /**
   * Create an instance; There's no magic here.
   */
  public function allocate() {
    return $this->create();
  }

}
