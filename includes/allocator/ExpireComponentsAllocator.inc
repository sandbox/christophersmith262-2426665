<?php
/**
 * @file
 * Defines a generic object allocator.
 */

/**
 * An abstract base class to define concrete allocator implementations off of.
 *
 * Each allocator implementation can be configured to produce an object
 * arguments.
 *
 * Allocators can be used a prototypes to create other allocators.
 *
 * It is the job of the allocator to serve instances of plugins, which each
 * allocator may do differently. In other words, changing the allocator
 * implementation used by a plugin allows us to swap out the creational design
 * pattern used to create plugin instances for that type.
 */
abstract class ExpireComponentsAllocator {
  protected $pluginData;
  protected $pluginType;
  protected $params;

  /**
   * Build an allocator for a given plugin type.
   *
   * In general, this can be used to build an allocator prototype. When
   * an allocator is needed for a specific set of
   *
   * @param string $plugin_type
   *   The type of plugin the allocator object will create.
   */
  public function __construct($plugin_type) {
    $this->pluginType = $plugin_type;
    $this->params = array();
  }

  /**
   * Creates an allocator from a prototype instance.
   *
   * @param ExpireComponentsPluginData $plugin_data
   *   Data to provide to instances that will be allocated by the newly created
   *   allocator. If NULL is given then the plugin data associated with the
   *   prototype will be used for the new allocator.
   *
   * @return ExpireComponentsAllocator
   *   An allocator object that will be cloned from the prototype.
   */
  public function copyFromPrototype(ExpireComponentsPluginData $plugin_data = NULL) {
    $allocator = clone $this;
    if ($plugin_data) {
      $allocator->pluginData = $plugin_data;
    }
    return $allocator;
  }

  /**
   * Sets a parameter for the allocator.
   *
   * @param string $key
   *   The parameter to set.
   * @param mixed $value
   *   The value of the parameter to set.
   *
   * @note a parameter is a property of the allocator. It is not passed along to
   * the created instance. Parameters are used to provide additional context to
   * an allocator to help it create objects. Any data needed by the created
   * objects needs to be in a ExpireComponentsPluginData instance.
   *
   * A use case of this is with the 'bin' allocator, which which organizes
   * instances into bins. A parameter is set to tell the argument what data it
   * should look at to sort the instances into bins.
   */
  public function setParam($key, $value) {
    $this->params[$key] = $value;
  }

  /**
   * Gets a stored parameter.
   *
   * @param string $key
   *   The parameter to get.
   *
   * @return mixed
   *   The value of the requested parameter, or NULL if it was not found.
   *
   * @see setParam()
   */
  public function getParam($key) {
    return isset($this->params[$key]) ? $this->params[$key] : NULL;
  }

  /**
   * Gets the plugin data associated with an allocator.
   *
   * @note This method allows users to minipulate the data that gets passed to
   * plugins before the plugin is created.
   *
   * @see ExpireComponentsPluginData
   *
   * @return ExpireComponentsPluginData
   *   The data object that will be passed to all instances created by this
   *   allocator.
   */
  public function args() {
    if (!is_object($this->pluginData)) {
      throw new ExpireComponentsException('Invalid plugin object', EXPIRE_COMPONENTS_ERR_ALLOC, $this);
    }
    return $this->pluginData;
  }

  /**
   * Fetches an instance of a plugin.
   *
   * This could mean fetching an existing instance or completely creating a new
   * instance depending on the concrete allocator implementation.
   *
   * @return ExpireComponentsPluginInterface
   *   An instance of a plugin.
   */
  public function inst() {
    return $this->allocate();
  }

  /**
   * Internal wrapper for creating instances.
   *
   * This always creates a new intstance and is only a helper method for
   * concrete classes.
   *
   * @return ExpireComponentsPluginInterface
   *   An instance of a plugin with the plugin data that has been configured for
   *   this allocator instance, or NULL if no valid plugin could be created.
   */
  protected function create() {
    if (class_exists($this->pluginData->type())) {
      $class = $this->pluginData->type();
      return new $class($this->pluginData);
    }
    else {
      throw ExpireComponentsException::create('Could not create plugin', EXPIRE_COMPONENTS_ERR_ALLOC, $this);
    }
    return $class;
  }

  /**
   * Implement this method method to create a specific allocation scheme.
   *
   * Concrete classes can call the create() method to build an actual instance
   * of the object.
   *
   * @return ExpireComponentsPluginInterface
   *   A plugin created by create().
   */
  abstract protected function allocate();

}
