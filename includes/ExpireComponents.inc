<?php
/**
 * @file
 * Defines utility functions for working with the component expiration system.
 */

/**
 * Implements a static interface for the component expiration system.
 *
 * The main duty of this class is providing developer friendly wrappers to
 * minipulate and create plugins and general state data.
 */
class ExpireComponents {
  static protected $plugins = NULL;

  /**
   * Initialize the ExpireComponents plugin system.
   *
   * By default this module will automatically handle allocation of the plugin
   * factory that will be used to create plugin instances, but we provide the
   * ability for users to specifically set the plugin instance to be used for a
   *
   * @param ExpireComponentsPluginFactory $plugins
   *   A plugin factory object to use for creating plugins. If none is given,
   *   one will be created with the settings configured through the admin UI.
   *   page request.
   *
   * @note If the caller manually sets a plugin factory object to use, then the
   * caller is responsible for settings up the object with the plugin
   * implementation to use for a given plugin type. If none is given then the
   * defaults will be used.
   */
  static public function init(ExpireComponentsPluginFactory $plugins = NULL) {
    if (!$plugins) {
      // Get a list of plugin types we wish to support with the
      // ExpireComponentsPluginFactory. We allow other modules to
      // alter the list so they can change allocators, defaults,
      // etc, for a given plugin type.
      module_load_include('inc', 'expire_components', 'expire_components.plugins');
      $plugin_types = _expire_components_plugin_types();
      drupal_alter('expire_components_plugin_types', $plugin_types);
      // Create a plugin factory instance for building plugin instances.
      $class = self::get('factory_class', 'ExpireComponentsPluginFactory');
      $plugins = new $class($plugin_types);
      // Set which plugins to use for a given type based on the
      // module configuration.
      $plugins->usePlugin('manager', self::get('manager'));
      $plugins->usePlugin('router', self::get('router'));
      $plugins->usePlugin('storage', self::get('storage'));
      self::$plugins = $plugins;
    }
    else {
      self::$plugins = $plugins;
    }
  }

  /**
   * Wrapper to load a plugin instance.
   *
   * @param mixed $plugin
   *   If this is a string, then it is treated as the plugin type, and the
   *   plugin implementation that is currently being used for that type will be
   *   used. If there is no plugin in use for the type then the default will be
   *   used. If this is an array then we use the 'type' key to specify the
   *   plugin type, and the 'name' key to specify wihich implementation to use.
   *
   * @return ExpireComponentsPluginInterface
   *   The loaded plugin object.
   */
  static public function plugin($plugin) {
    if (!self::$plugins) {
      self::init();
    }
    $allocator = self::$plugins->getAllocator($plugin);
    if (!$allocator) {
      self::watchdogError('Could not load plugin @plugin', array('@plugin' => $plugin));
    }
    return $allocator->inst();
  }

  /**
   * Adds a component to the component list for the current page.
   *
   * @param string $component_type
   *   The type of component to track. This should be the key for the
   *   ExpireComponentsPlugin implementation that will be used to handle
   *   expiration of the component.
   * @param mixed $data
   *   An arbitrary pice of data that will get stored in the list containing any
   *   information needed to expire the component.
   */
  static public function track($component_type, $data) {
    self::plugin('router')->currentPath()->addComponent($component_type, $data);
  }

  /**
   * Log a message to watchdog (as a notice).
   *
   * @see watchdog()
   *
   * @note do not pass translated strings into this function. Use
   * format_string() instead. For more information on why, consult the
   * watchdog() function documentation on Drupal.org.
   */
  static public function watchdogWarning($msg, $vars = array(), $severity = WATCHDOG_NOTICE) {
    watchdog('expire_components', $msg, $vars, $severity);
  }

  /**
   * Log an error to watchdog (as an error).
   *
   * @note do not pass translated strings into this function. Use
   * format_string() instead. For more information on why, consult the
   * watchdog() function documentation on Drupal.org.
   *
   * @see watchdog()
   */
  static public function watchdogError($msg, $vars = array(), $severity = WATCHDOG_ERROR) {
    watchdog('expire_components', $msg, $vars, $severity);
  }

  /**
   * Log a debug message based on the expire module configuration.
   *
   * @see watchdog()
   * @see drupal_set_message()
   *
   * @note This function does not support passing $vars since the
   * drupal_set_message() function does not support it. As such, it requires
   * that $msg strings be translated before being passed in. This makes for some
   * incongruity between how watchdog() handles strings, and how
   * drupal_set_message() handles strings, but since this is only for debug
   * purposes, we assume that storing messages in translated form is fine.
   *
   * This function depends on the debug settings for the Cache Expiration module
   * that can be configured through the UI.
   */
  static public function debug($msg, $severity = WATCHDOG_DEBUG) {
    $debug = variable_get('expire_debug', EXPIRE_DEBUG_DISABLED);
    if (empty($debug)) {
      return;
    }

    watchdog('expire_components', $msg, array(), $severity);

    if ($debug == EXPIRE_DEBUG_FULL) {
      drupal_set_message(check_plain($msg));
    }
  }

  /**
   * Flushes all component lists.
   */
  static public function flushAllComponents() {
    self::plugin('storage')->flushAll();
  }

  /**
   * Gets a list of plugins for a given plugin type.
   *
   * This allows the ability to easily adding options to a drupal form element
   * (select, radios, checkboxes, etc).
   *
   * @param string $plugin_type
   *   The plugin type to lookup.
   *
   * @return object
   *   A stdClass object with the following keys:
   *    'options' contains a list of options for use in a drupal form.
   *    'default' a default value if supported by the plugin.
   *    'description' an array of descriptions, one for each of the options
   */
  static public function options($plugin_type) {
    if (!self::$plugins) {
      self::init();
    }
    // Create a stdClass object to hold the option data.
    $rtn = new stdClass();
    $rtn->options = array();
    $rtn->default = NULL;
    $rtn->description = array();
    // Get default values if possible.
    $default = self::get($plugin_type);
    if ($default) {
      $rtn->default = $default;
    }
    else {
      $type_info = self::$plugins->typeInfo($plugin_type);
      if ($type_info && isset($type_info['default'])) {
        $rtn->default = $type_info['default'];
      }
    }
    // Build out options and description.
    foreach (self::$plugins->plugins($plugin_type) as $plugin_key => $plugin) {
      $log = array('@key' => $plugin_key);
      if (isset($plugin['name'])) {
        $log = array('@name' => $plugin['name']);
        $rtn->options[$plugin_key] = t('@name', array('@name' => $plugin['name']));
        if (isset($plugin['description'])) {
          $log['@desc'] = $plugin['description'];
          $rtn->description[] = t('@name: @desc', $log);
        }
      }
    }
    return $rtn;
  }

  /**
   * Gets a drupal variable for the expire component module.
   *
   * @param string $key
   *   The variable key.
   * @param mixed $default
   *   A default value if the variable is not set.
   *
   * @return string
   *   The requested setting value.
   */
  static public function get($key, $default = NULL) {
    return variable_get('expire_components_' . $key, $default);
  }

  /**
   * Sets a drupal variable for the expire component module.
   *
   * @param string $key
   *   The key to store the value for.
   * @param mixed $val
   *   The value to store.
   *
   * @note Calling this any time other than on an admin form
   * submit is bad news bears. Don't do it.
   */
  static public function set($key, $val) {
    variable_set('expire_components_' . $key, $val);
  }

}
