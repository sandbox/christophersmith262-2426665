<?php
/**
 * @file
 * Provides grouping cache expiration component plugins and component data.
 *
 * @see ExpireComponentsQueue
 */

/**
 * Groups a cache clearing handler with data needed to expire the cache.
 *
 * At its core this just groups a cache invalidation action
 * with the data it is acting on.
 */
class ExpireComponentsExpireQueueItem implements ExpireComponentsQueueItemInterface {
  protected $jobId;
  protected $handler;
  protected $data;

  /**
   * Creates a queue item.
   *
   * @param ExpireComponentsComponentInterface $handler
   *   The component plugin that will be used to clear the cache.
   * @param array $data
   *   An associative array of data needed to clear the cache.
   */
  public function __construct($job_id, ExpireComponentsComponentInterface $handler, array $data) {
    $this->jobId = $job_id;
    $this->handler = $handler;
    $this->data = $data;
  }

  /**
   * Pass along the job id (which is the component key).
   *
   * @see ExpireComponentsJobQueue::jobId()
   */
  public function jobId() {
    return $this->jobId;
  }

  /**
   * Processes a queue item by expiring its contents.
   *
   * @see ExpireComponentsJobQueue::process()
   */
  public function process() {
    $this->handler->expire($this->data);
    ExpireComponents::debug(t('Expired @path', array('@path' => $this->jobId())));
  }

}
