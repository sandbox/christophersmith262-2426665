<?php
/**
 * @file
 * Provides an interface for a job queue item.
 */

/**
 * Items that can be processed by a job queue should implement this interface.
 */
interface ExpireComponentsQueueItemInterface {

  /**
   * Tells the job queue the job id of the current job.
   *
   * Job ids are a unique value associated with the queue item. Queueing based
   * on job id prevents us from ever running the same job id twice for a given
   * queue.
   *
   * @return mixed
   *   The job id for this item.
   */
  public function jobId();

  /**
   * Executes a job.
   *
   * @return mixed
   *   A the result of the job.
   */
  public function process();

}
