<?php
/**
 * @file
 * Provides queue functionality for back end storage searching.
 *
 * @see ExpireComponentsQueue
 */

/**
 * Defines a job queue item for performing path lookups.
 */
class ExpireComponentsSearchQueueItem implements ExpireComponentsQueueItemInterface {
  protected $path;
  protected $wildcard;
  protected $normalized;
  protected $storage;

  /**
   * Creates a path lookup queue item.
   *
   * @param string $path
   *   The path to be looked up.
   * @param ExpireComponentsRouter $router
   *   The router object to use to normalize the path.
   * @param ExpireComponentsStorageInterface $storage
   *   The storage back end to look up with.
   */
  public function __construct($path, ExpireComponentsRouter $router, ExpireComponentsStorageInterface $storage) {
    $this->path = preg_replace('/|wildcard$/', '', $path, -1, $count);
    $this->wildcard = $count ? TRUE : FALSE;
    $this->normalized = $router->normalize($this->path);
    $this->storage = $storage;
  }

  /**
   * The search key to match in the back end lookup operation.
   *
   * @return string
   *   The normalized path associated with this search object.
   */
  public function searchKey() {
    return $this->normalized;
  }

  /**
   * Determines if a wildcard search should be done.
   *
   * A wild card search instructs the storage back end to search for anything
   * beginning with the search key.
   *
   * @return bool
   *   TRUE if this is a wildcard search, FALSE otherwise.
   */
  public function isWildcard() {
    return $this->wildcard;
  }

  /**
   * Instructs the queue to use the search key as the job id.
   *
   * @see ExpireComponentsJobQueue::jobId()
   */
  public function jobId() {
    return $this->searchKey();
  }

  /**
   * Performs the actual search operation.
   *
   * @see ExpireComponentsJobQueue::process()
   */
  public function process() {
    return $this->storage->search($this);
  }

}
