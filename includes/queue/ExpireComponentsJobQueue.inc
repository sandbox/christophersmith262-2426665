<?php
/**
 * @file
 * Defines a class representing a queue of items to be expired.
 */

/**
 * A class to store a queue of items that need to be processed for expiration.
 */
class ExpireComponentsJobQueue {
  protected $queue;

  /**
   * Creates a queue.
   */
  public function __construct() {
    $this->queue = array();
  }

  /**
   * Pushes an item on the back of the queue.
   */
  public function push(ExpireComponentsQueueItemInterface $item) {
    $this->queue[$item->jobId()] = $item;
  }

  /**
   * Processes all the items in the queue.
   */
  public function process() {
    $results = array();
    foreach ($this->queue as $job_id => $item) {
      // Catch exceptions for each individual job
      // that needs to be processed. That way if an
      // exception occurs while processing a given
      // job the entire queue processing chain
      // doesn't get nuked.
      try {
        $results[$job_id] = $item->process();
      }
      catch (Exception $e) {
        ExpireComponents::error('Error processing @item:@job: @msg',
          array(
            '@item' => get_class($item),
            '@job' => $item->jobId(),
            '@msg' => $e->getMessage(),
          )
        );
      }
    }
    return $results;
  }

}
