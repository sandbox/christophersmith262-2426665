<?php
/**
 * @file
 * Provides a custom exception object for plugins.
 */

/**
 * Defines an exception subclass for ExpireComponents related exceptions.
 *
 * This allows us to attach arbitrary data about the conditions that created the
 * exception.
 */
class ExpireComponentsException extends Exception {
  public $data;

  /**
   * Creates an exception object.
   *
   * @param string $msg
   *   A log message about he exception.
   * @param int $code
   *   An integer code describing the exception.
   * @param object $owner
   *   An optional argument specifying the object that
   *   threw the exception.
   */
  public function __construct($msg, $code, $owner = NULL) {
    $this->data = array();
    parent::__construct($msg, $code);
    if ($owner) {
      $this->context('owner', $owner);
    }
  }

  /**
   * Attaches a piece of arbitrary data to the exception.
   *
   * @param mixed $key
   *   The name of the data being attached.
   * @param mixed $val
   *   The data to attach.
   */
  public function context($key, $val) {
    $this->data[$key] = $val;
  }

}
