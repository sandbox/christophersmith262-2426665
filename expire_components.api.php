<?php
/**
 * @file
 * API Examples.
 */

/**
 * Add a component cache clearing plugins.
 */
function hook_expire_components_plugins() {
  $plugins['my_custom_widget_expire_plugin'] = array(
    'name' => t('Custom Widget'),
    'type' => 'component',
    'description' => t('Clear the custom widget cache when a page is expired'),
    'handler' => array(
      'class' => 'ExpireComponentsCustomWidget',
    ),
  );
  return $plugins;
}

/**
 * Alter existing component cache clearing plugins.
 */
function hook_expire_components_plugins_alter(&$plugins) {
  $plugins['handler']['my_custom_widget_expire_plugin']['class'] = 'ExpireComponentsCustomWidget';
}

/**
 * Alter the definition of a plugin type.
 *
 * @see expire_components.plugins.inc
 */
function hook_expire_components_plugin_types_alter(&$plugin_types) {
  $plugin_types['component']['allocator'] = 'ExpireComponentsStandardAllocator';
}
